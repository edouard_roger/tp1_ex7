# -- coding: utf-8 --

"""
Created on Fri Mar 13 15:10:17 2020

@author: edouard.roger
"""

import unittest
import logging

from utils.operations import SimpleCalculator


class Test(unittest.TestCase):
    def test_addition(self):

        CALC = SimpleCalculator(10, 2)
        SOMME = CALC.sum()
        self.assertEqual(SOMME, 12)

        CALC = SimpleCalculator(2, "10")
        SOMME = CALC.sum()
        self.assertEqual(SOMME, "ERROR")

        CALC = SimpleCalculator(-2, -10)
        SOMME = CALC.sum()
        self.assertEqual(SOMME, -12)
        self.assertNotEqual(SOMME, 12)

        logging.warning("test_addition ok warn")
        logging.info("test_addition ok inf")

    def test_substration(self):

        CALC = SimpleCalculator(10, 2)
        SOUSTRACTION = CALC.subtract()
        self.assertEqual(SOUSTRACTION, 8)

        CALC = SimpleCalculator(2, "10")
        SOUSTRACTION = CALC.subtract()
        self.assertEqual(SOUSTRACTION, "ERROR")

        CALC = SimpleCalculator(-2, -10)
        SOUSTRACTION = CALC.subtract()
        self.assertEqual(SOUSTRACTION, 8)
        self.assertNotEqual(SOUSTRACTION, -12)

        logging.warning("test_substraction ok warn")
        logging.info("test_substraction ok inf")

    def test_multiplication(self):

        CALC = SimpleCalculator(10, 2)
        MULTIPLICATION = CALC.multiply()
        self.assertEqual(MULTIPLICATION, 20)

        CALC = SimpleCalculator(2, "10")
        MULTIPLICATION = CALC.subtract()
        self.assertEqual(MULTIPLICATION, "ERROR")

        CALC = SimpleCalculator(-2, -10)
        MULTIPLICATION = CALC.multiply()
        self.assertEqual(MULTIPLICATION, 20)
        self.assertNotEqual(MULTIPLICATION, -20)

        logging.warning("test_multiplication ok warn")
        logging.info("test_multiplication ok inf")

    def test_division(self):

        CALC = SimpleCalculator(10, 2)
        DIVISION = CALC.divide()
        self.assertEqual(DIVISION, 5)

        CALC = SimpleCalculator(2, "10")
        DIVISION = CALC.divide()
        self.assertEqual(DIVISION, "ERROR")

        CALC = SimpleCalculator(-10, -2)
        DIVISION = CALC.divide()
        self.assertEqual(DIVISION, 5)
        self.assertNotEqual(DIVISION, -5)

        with self.assertRaises(ZeroDivisionError):
            CALC.divide(10, 0)

        logging.warning("test_division ok warn")
        logging.info("test_division ok inf")


unittest.main()
